var app = angular.module("app", []);
/**
 * This service set configuration data for cup according to cup size.
 * 
 */
app.service('CupService', function ($rootScope) {
    this.config = {};
    this.config.leftCanvas = {};
    this.config.rightCanvas = {};
    this.config.centerCanvas = {}
    this.config.templateImage = "baseimages/template.png";
    this.config.cupInch = 4;
    this.config.renderImageHeight = 250;
    this.setCupType = function (type) {

        if (type == $rootScope.cup11oz) {
            this.config.leftCanvas.src = "baseimages/11oz_left.png";
            this.config.leftCanvas.xOffset = 148;
            this.config.leftCanvas.yOffset = 122;
            this.config.leftCanvas.imageWidth = 110;
            this.config.leftCanvas.roundNess = 15;
            this.config.leftCanvas.wrapHeight = 355;

            this.config.rightCanvas.src = "baseimages/11oz_right.png";
            this.config.rightCanvas.xOffset = 128;
            this.config.rightCanvas.yOffset = 122;
            this.config.rightCanvas.imageWidth = 110;
            this.config.rightCanvas.roundNess = 15;
            this.config.rightCanvas.wrapHeight = 355;

            this.config.centerCanvas.src = "baseimages/11oz_center.png";
            this.config.centerCanvas.xOffset = 150;
            this.config.centerCanvas.yOffset = 122;
            this.config.centerCanvas.imageWidth = 110;
            this.config.centerCanvas.roundNess = 15;
            this.config.centerCanvas.wrapHeight = 355;


        } else if (type == $rootScope.cup15oz) {
            this.config.leftCanvas.src = "baseimages/15oz_left.png";
            this.config.leftCanvas.xOffset = 162;
            this.config.leftCanvas.yOffset = 75;
            this.config.leftCanvas.imageWidth = 112;
            this.config.leftCanvas.roundNess = 20;
            this.config.leftCanvas.wrapHeight = 400;

            this.config.rightCanvas.src = "baseimages/15oz_right.png";
            this.config.rightCanvas.xOffset = 112;
            this.config.rightCanvas.yOffset = 75;
            this.config.rightCanvas.imageWidth = 112;
            this.config.rightCanvas.roundNess = 20;
            this.config.rightCanvas.wrapHeight = 400;

            this.config.centerCanvas.src = "baseimages/15oz_center.png";
            this.config.centerCanvas.xOffset = 137;
            this.config.centerCanvas.yOffset = 75;
            this.config.centerCanvas.imageWidth = 112;
            this.config.centerCanvas.roundNess = 20;
            this.config.centerCanvas.wrapHeight = 400;

        }
    };

    this.getLeftCupConfig = function () {
        return this.config.leftCanvas;
    };
    this.getCenterCupConfig = function () {
        return this.config.centerCanvas;
    };
    this.getRightCupConfig = function () {
        return this.config.rightCanvas;
    };
    this.getTemplateImage = function () {
        return this.config.templateImage;
    };
    this.getLeftCupImage = function () {
        return this.config.leftCanvas.src;
    };
    this.getRightCupImage = function () {
        return this.config.rightCanvas.src;
    };
    this.getCenterCupImage = function () {
        return this.config.centerCanvas.src;
    };
    this.getCupInch = function () {
        return this.config.cupInch;
    };
    this.getRanderImageHeight = function () {
        return this.config.renderImageHeight;
    };

});
app.controller("ImagePocController", function ($scope, CupService, $rootScope, $timeout) {
    var SelectedImage;
    var canvas, canvasCombine, ctxCombine;
    var left, right, center;

    /**
     * This function will be called when user choose size of cup.
     * 
     */
    $scope.setCupType = function (type) {
        CupService.setCupType(type);
    }
    /**
     * 
     * This function will be called after controller loaded.
     * It is used to set template image and binded function for process,remove and download the image.
     */
    $scope.loadAllCanvas = function () {
        $rootScope.cup11oz = "11z";
        $rootScope.cup15oz = "15z";
        $rootScope.GOOD = "Good /";
        $rootScope.AVERAGE = "Average /";
        $rootScope.POOR = "Poor /";

        canvasCombine = document.getElementById("canvasAll");
        ctxCombine = canvasCombine.getContext("2d");

        canvas = new fabric.Canvas('imageCanvas', {
            backgroundColor: 'rgb(240,240,240)'
        });
        var baseImage = CupService.getTemplateImage();
        canvas.setBackgroundImage(baseImage, canvas.renderAll.bind(canvas));

        var imageLoader = document.getElementById('imageLoader');
        imageLoader.addEventListener('change', handleImage, false);
        canvas.on({
            'object:moving': function (e) {
            },
            'object:modified': function (e) {
//                console.log(e);
                var act = canvas.getActiveObject();
                //  console.log("Actual Height="+e.target.height);
                // console.log("Actual Widht="+e.target.width);
                $scope.$apply(function () {
                    $scope.dpi = e.target.height / ((act.getHeight() / CupService.getRanderImageHeight()) * CupService.getCupInch());
                    if ($scope.dpi > 300) {
                        $scope.printQuality = $rootScope.GOOD;
                    } else if ($scope.dpi > 250) {
                        $scope.printQuality = $rootScope.AVERAGE;
                    } else {
                        $scope.printQuality = $rootScope.POOR;
                    }

                });

//                console.log("Dpi=" + act.getHeight()/4);
                // console.log("Widht=" + act.getWidth());
                // console.log("Left=" + act.getLeft());
                // console.log("Top=" + act.getTop());
                // console.log("Angle=" + act.getAngle());
                // console.log("-------------");
                // act.setHeight(200);
                // var oImg = act.set({left: 0, top: 0, angle: 00, width: 100, height: 100}).scale(0.9);
                // canvas.add(oImg).renderAll();
                // canvas.setActiveObject(oImg);
//                  var object = canvas.getActiveObject();
//                  console.log(object.getSrc());
            },
            'object:scaling': function (e) {
                var act = canvas.getActiveObject();
                //  console.log("Actual Height="+e.target.height);
                // console.log("Actual Widht="+e.target.width);
                $scope.$apply(function () {
                    $scope.dpi = e.target.height / ((act.getHeight() / CupService.getRanderImageHeight()) * CupService.getCupInch());
                    if ($scope.dpi > 300) {
                        $scope.printQuality = $rootScope.GOOD;
                    } else if ($scope.dpi > 250) {
                        $scope.printQuality = $rootScope.AVERAGE;
                    } else {
                        $scope.printQuality = $rootScope.POOR;
                    }
                });
            }
        });
        var imageSaver = document.getElementById('imageSaver');
        imageSaver.addEventListener('click', processImage, false);

        var removeImage1 = document.getElementById('imageRemove1');
        removeImage1.addEventListener('click', removeImage, false);

        var downImage = document.getElementById('downloadimage');
        downImage.addEventListener('click', downloadImage, false);


    }
    /**
     * This function used to convert canvas to Image
     * @param {type} background=sets the background of the canvas when converting it into Image
     * @param {type} canvas =which is going to convert into image
     * @param {type} context=This is context of canvas.
     * @returns {unresolved}
     */
    function canvasToImage(background, canvas, context)
    {
        //cache height and width		
        var w = canvas.width;
        var h = canvas.height;

        var data;
        if (background)
        {
            //get the current ImageData for the canvas.
            data = context.getImageData(0, 0, w, h);

            //store the current globalCompositeOperation
            var compositeOperation = context.globalCompositeOperation;

            //set to draw behind current content
            context.globalCompositeOperation = "destination-over";

            //set background color
            context.fillStyle = background;

            //draw background / rect on entire canvas
            context.fillRect(0, 0, w, h);
        }

        //get the image data from the canvas
        var imageData = canvas.toDataURL("image/png");

        if (background)
        {
            //clear the canvas
            context.clearRect(0, 0, w, h);

            //restore it with original / cached ImageData
            context.putImageData(data, 0, 0);

            //reset the globalCompositeOperation to what it was
            context.globalCompositeOperation = compositeOperation;
        }

        //return the Base64 encoded data url string
        return imageData;
    }
    /**
     * This function will be called after user specified image wrapped around the left,center and right cup respectively.
     * It is used to combined all Images in single canvas except image which does not have any wrapped content
     */
    function combineAll(img) {

        if (ctxCombine) {
            ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
        }
        var img1 = loadImage(left, main);
        var img2 = loadImage(center, main);
        var img3 = loadImage(right, main);
        var imagesLoaded = 0;

        function main() {
            imagesLoaded += 1;
            var iw = img1.width;
            var ih = img1.height;
            var firstImageFound = false;
            var secondImageFound = false;
            var firstImageFound = false;
            if (imagesLoaded == 3) {
                var leftCupImage = CupService.getLeftCupImage();
                resemble(left).compareTo(leftCupImage).ignoreColors().onComplete(function (data) {
                    var misMatchPer = parseFloat(data.misMatchPercentage);
                    if (misMatchPer > 7.80) {
                        ctxCombine.drawImage(img1, 0, 0, iw, ih);
                        firstImageFound = true;
                    }
                    var centerCupImage = CupService.getCenterCupImage();
                    resemble(center).compareTo(centerCupImage).ignoreColors().onComplete(function (data) {

                        var misMatchPer = parseFloat(data.misMatchPercentage);
                       // console.log(misMatchPer);
                        if (misMatchPer > 7.91) {
                            if (firstImageFound == true) {
                                ctxCombine.drawImage(img2, iw, 0, iw, ih);
                            } else {
                                ctxCombine.drawImage(img2, 0, 0, iw, ih);
                            }
                            secondImageFound = true;
                        }
                        var rightCupImage = CupService.getRightCupImage();
                        resemble(right).compareTo(rightCupImage).ignoreColors().onComplete(function (data) {
                            var misMatchPer = parseFloat(data.misMatchPercentage);
                            if (misMatchPer > 7.91) {
                                if (firstImageFound == true && secondImageFound == true) {
                                    ctxCombine.drawImage(img3, iw + iw, 0, img3.width, ih);
                                } else if (firstImageFound == false && secondImageFound == false) {
                                    ctxCombine.drawImage(img3, 0, 0, iw, ih);
                                } else {
                                    ctxCombine.drawImage(img3, iw, 0, iw, ih);
                                }
                            }

                        });


                    });

                });




            }

        }

        function loadImage(src, onload) {
            var img = new Image();

            img.onload = onload;
            img.src = src;

            return img;
        }
    }
    /**
     * This function will be called when user clicked on Download Image Button
     * It will download all the Cups in single image.
     */
    function downloadImage() {
        var link = document.createElement('a');
//        link.href = canvasCombine.toDataURL({
//            format: 'png',
//            quality: 0.8
//        });
        link.href = canvasToImage("rgba(255,255,255,1)", canvasCombine, ctxCombine);
        link.download = 'finalImage.png';
        // document.body.appendChild(link);
        link.click();
    }
    /**
     * This function will be called when user select image file for wrapping purpose.
     */
    function handleImage(e) {
        var reader = new FileReader();
        var actualHeight;
        reader.onload = function (event) {
            var img = new Image();
            img.onload = function () {
                var imgInstance = new fabric.Image(img, {
                    scaleX: 0.2,
                    scaleY: 0.2,
                    left: 20,
                    top: 20
                })
                canvas.add(imgInstance);
                var active = canvas.setActiveObject(imgInstance);

            }
            img.src = event.target.result;
            actualHeight = img.height;

        }
        reader.readAsDataURL(e.target.files[0]);
        SelectedImage = reader;
        $timeout(function () {
            var act = canvas.getActiveObject();
            $scope.dpi = actualHeight / ((act.getHeight() / CupService.getRanderImageHeight()) * CupService.getCupInch());
            if ($scope.dpi > 300) {
                $scope.printQuality = $rootScope.GOOD;
            } else if ($scope.dpi > 250) {
                $scope.printQuality = $rootScope.AVERAGE;
            } else {
                $scope.printQuality = $rootScope.POOR;
            }
        }, 500);
    }

    /**
     * This function will be called when user clicked on remove image button.
     * It is used to remove image from template view.
     */
    function removeImage(e) {

        var object = canvas.getActiveObject();
        if (!object) {
            alert('Please select the element to remove');
            return '';
        }
        canvas.remove(object);
    }
    /**
     * This function will be called when user clicked on Process Image Button
     * 
     */
    function processImage(e) {
        left = "";
        right = "";
        center = "";
        if (ctxCombine) {
            ctxCombine.clearRect(0, 0, canvasCombine.width, canvasCombine.height);
        }
        var cupConfig = CupService.getLeftCupConfig();
        if (angular.isDefined(cupConfig.src)) {
            canvas.backgroundImage = 0;
            canvas.setBackgroundColor("white", canvas.renderAll.bind(canvas));
            var img = canvas.toDataURL({
                format: 'jpeg',
                quality: 0.8
            });
            wrappImageOnLeftCup(img);
            wrappImageOnCenterCup(img);
            wrappImageOnRightCup(img);
            setTimeout(function () {
                canvas.setBackgroundImage('baseimages/template.png', canvas.renderAll.bind(canvas));
                combineAll();
                //  myImg.src = white2transparent(myImg);
            }, 1000);
            //}, 10);
        } else {
            alert("Please choose size of cup.");
        }
    }

    /**
     * This function is used to wrap Image around left cup
     */
    function wrappImageOnLeftCup(img) {
        var canvas = document.createElement("canvas");//document.getElementById("canvas1");
        var ctx = canvas.getContext("2d");

        var productImg = new Image();
        var cupConfig = CupService.getLeftCupConfig();

        productImg.onload = function () {
            var iw = productImg.width;
            var ih = productImg.height;
            canvas.width = iw;
            canvas.height = ih;

            ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                    0, 0, iw, ih);
            loadUpperIMage(img);


        };

        productImg.src = cupConfig.src;


        function loadUpperIMage(img1) {
            var img = new Image();
            img.src = img1;
            img.onload = function () {

                var iw = img.width;
                var ih = img.height;
                var xOffset = cupConfig.xOffset;// 148, //left padding
                var yOffset = cupConfig.yOffset; //125; //top padding

                var a = cupConfig.imageWidth;// 110.0; //image width
                var b = cupConfig.roundNess;//10; //round ness

                var scaleFactor = iw / (4 * a);
                // draw vertical slices
                var counter = 0;
                //  ctx.globalAlpha = 0.9;
                for (var X = 0; X < iw; X += 1) {
                    var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
//                    ctx.globalAlpha = 0.9;
                    //void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                    ctx.drawImage(img, X * scaleFactor, 0, iw / 9, ih, X + xOffset, y + yOffset, 1, cupConfig.wrapHeight);
                    ctx.globalCompositeOperation = "multiply";

                }

                left = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });

            };
//        return new Promise(
//                function (resolve, reject) {
//                    resolve("finished");
//                });
        }
    }

    /**
     * This function is used to wrap Image around Center cup
     */
    function wrappImageOnCenterCup(img1) {

        var canvas = document.createElement("canvas");// document.getElementById("canvas2");
        var ctx = canvas.getContext("2d");

        var productImg = new Image();
        var cupConfig = CupService.getCenterCupConfig();
        productImg.onload = function () {
            var iw = productImg.width;
            var ih = productImg.height;

            canvas.width = iw;
            canvas.height = ih;

            ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                    0, 0, iw, ih);
            loadUpperIMage(img1)
        };


        productImg.src = cupConfig.src;

        function loadUpperIMage(img1) {
            var img = new Image();

            img.src = img1;
            img.onload = function () {

                var iw = img.width;
                var ih = img.height;

                // alert(iw)

                var xOffset = cupConfig.xOffset;//150, //left padding
                var yOffset = cupConfig.yOffset;//125; //top padding


                var a = cupConfig.imageWidth;// 110.0; //image width
                var b = cupConfig.roundNess;//10; //round ness

                var scaleFactor = iw / (4 * a);

                // draw vertical slices
                for (var X = 0; X < iw; X += 1) {
                    var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                    ctx.globalAlpha = 0.9;
                    ctx.drawImage(img, X * scaleFactor, 0, iw / 3, ih, X + xOffset, y + yOffset, 1, cupConfig.wrapHeight);
                    ctx.globalCompositeOperation = "multiply";



                }

                center = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });


            };
        }

    }

    /**
     * This function is used to wrap Image around Right cup
     */
    function wrappImageOnRightCup(img1) {

        var canvas = document.createElement("canvas");// document.getElementById("canvas3");
        var ctx = canvas.getContext("2d");
        var cupConfig = CupService.getRightCupConfig();

        var productImg = new Image();
        productImg.onload = function () {
            var iw = productImg.width;
            var ih = productImg.height;

            canvas.width = iw;
            canvas.height = ih;

            ctx.drawImage(productImg, 0, 0, productImg.width, productImg.height,
                    0, 0, iw, ih);
            loadUpperIMage(img1)
        };

        productImg.src = cupConfig.src;//"baseimages/11oz_right.png";//"http://res.cloudinary.com/pussyhunter/image/upload/h_350/right_handle_cup_dsdhr7.jpg"


        function loadUpperIMage(img1) {
            var img = new Image();

            img.src = img1;//"argusoft.png";//"http://res.cloudinary.com/pussyhunter/image/upload/v1488184107/500_F_97150423_M13q2FeAUZxxIx6CaPixHupprmyiVVli_skh6fe.jpg"

            img.onload = function () {

                var iw = img.width;
                var ih = img.height;

                //alert(iw)


                var xOffset = cupConfig.xOffset;//128, //left padding
                var yOffset = cupConfig.yOffset;//125; //top padding


                var a = cupConfig.imageWidth;// 115.0; //image width
                var b = cupConfig.roundNess;//10; //round ness
                var scaleFactor = iw / (3 * a);

                // draw vertical slices
                for (var X = 0; X < iw; X += 1) {
                    var y = b / a * Math.sqrt(a * a - (X - a) * (X - a)); // ellipsis equation
                    ctx.globalAlpha = 0.9;
                    ctx.drawImage(img, X * scaleFactor, 0, iw / 1.5, ih, X + xOffset, y + yOffset, 1, cupConfig.wrapHeight);
                    ctx.globalCompositeOperation = "multiply";


                }
                right = canvas.toDataURL({
                    format: 'jpeg',
                    quality: 0.8
                });


            };
        }

    }


});